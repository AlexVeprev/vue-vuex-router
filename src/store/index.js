import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        page: 1,
    },
    getters: {
        getPage: state => {
            return state.page;
        }
    },
    mutations: {
        setPage (state, n) {
            state.page = n;
        }
    },
    actions: {},
    modules: {}
})
